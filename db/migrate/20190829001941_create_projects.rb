class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string  :project_name
      
      t.string  :git_url
      t.integer :git_id

      t.string  :email_list
      t.integer :email_id

      t.string  :irc_channel
      t.string  :irc_server
      
      t.timestamps
    end
  end
end

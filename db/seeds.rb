require 'csv'

emails = []
CSV.foreach(Rails.root.join('lib/Email.csv'), headers: true) do |row|
    emails << row.to_h
end
Email.import(emails)

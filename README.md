# StarFLOSS API

The StarFLOSS API is the gateway responsible for aggregating data for the StarFLOSS initiative.
Its main objectve is to gather data about the commits of a project, the email list and possibly
the irc channel the comunity uses to communicate. Besides aggregte this api is also proccessing 
the data so it can provide data for graphical analysis.

[![pipeline status](https://gitlab.com/felipecaetanosilva/api/badges/master/pipeline.svg)](https://gitlab.com/felipecaetanosilva/api)

## Installing

Ruby Version: 2.6.3
Rails Version: 6.0.0

To install the dependencies of the project run: 

    $ cd cloned/repository
    $ bundle install

Then proceed to the creations of the tables used in the local database 
and migrate it with:

    $ rake db:create
    $ rake db:migrate
    
If you want to populate the Email database with the Git Mail list to play
with data run:

    $ rake db:seed
    
It may take a bunch of seconds and then you can test the api with:

    $ rails s
    
After this command the API, open your favorite browser and open the link:

    localhost:3000
    
You will be redirected to the Documentation Page. Have fun.


## Contributing

If you want to contribute to this project be welcome and read this Post about 
commit messages [this](https://chris.beams.io/posts/git-commit/). In this project
we try to follow this conventions but if you can show what you have done in 
your message you are free to try another format.

It is very polite to contact me first, so we can discuss things and syncronize 
our efforts before start coding!

And before sending your Merge Requests, please don't forget to run the 
following command to verify if you didn't break any work done.

    $ rake test
    
All the tests are ok. Nice! Wait a little time for a peer review and Voila! You
helped me to improve this piece of software.


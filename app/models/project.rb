class Project < ApplicationRecord
    validates :project_name, presence: true
    validates :git_url, presence: true
    validates :git_id, presence: true
end

require 'sentimental'

class Api::V1::IrcsController < ApplicationController
    before_action :set_project, only: [:ircs, :myircs, :statistics]

    @@judge = Sentimental.new
    @@judge.load_defaults
    # GET /projects/1/ircs
    api :GET, '/projects/:project_id/ircs', 'Get an array with all the irc messages from the irc channel of a project'
    returns :code => 200 do 
        property :_id, String, desc: "ObjectId of the mongo document"
        property :nick, :number, desc: 'Nick of the author of the message'
        property :message, String, desc: 'The body of the message'
        property :time, Date, desc: "Timestamp when the message was posted"
        property :channel, String, desc: "irc channel of the message"
        property :server, String, desc: "irc server of the message"
        property :sentiment, :number, desc: "numeric representation of the sentiment analysis"
    end
    def ircs
        begin
            ircs_response = RestClient.get ENV['BASE_URL'] + "irc/messages?channel=#{@project.irc_channel}&server=#{@project.irc_server}"
            responseHash = JSON.parse ircs_response.body
            responseHash.each do |irc|
                sentiment = @@judge.score irc["message"]
                irc["sentiment"] = sentiment
            end
            render json: responseHash, status: :ok         
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end


    # GET /projects/1/ircs/statistics
    api :GET, '/projects/:project_id/ircs/statistics',  'Get statistics of the irc channel'
    returns :code => 200 do 
        property :total_messages, :number, desc: 'Total number of messages from the channel'
        property :distinct_nicks, :number, desc: 'Number of distinct nicks to send messages'
        property :total_by_nick, Hash, desc: 'List of every message sender and its number of messages'
    end
    def statistics
        ircs_response = RestClient.get ENV['BASE_URL'] + "irc/messages?channel=#{@project.irc_channel}&server=#{@project.irc_server}"
        hash = JSON.parse ircs_response
        total_distinct = 0;
        contributors = hash.group_by{|e| e["nick"]}.map{|k, v| [k, v.length]}.to_h
        hash.group_by{|e| e["nick"]}.each{|k, v| total_distinct = total_distinct + 1}.to_h
        response = {
            :total_messages => hash.count,
            :distinct_nicks => total_distinct,
            :total_by_nick => contributors,
        }
        render json: response, status: :ok
    end

    api :POST, '/projects/:project_id/ircs/mymessages', 'Get an array with all the messages from specific nick'
    returns :code => 200 do 
        property :_id, String, desc: "ObjectId of the mongo document"
        property :nick, :number, desc: 'Nick of the author of the message'
        property :message, String, desc: 'The body of the message'
        property :time, Date, desc: "Timestamp when the message was posted"
        property :channel, String, desc: "irc channel of the message"
        property :server, String, desc: "irc server of the message"
    end
    def myMessages
        begin
            ircs_response = RestClient.get ENV['BASE_URL'] + "irc/messages?channel=#{@project.irc_channel}&server=#{@project.irc_server}&nick=#{user_nick["name"]}"
            response = ircs_response.body
            render json: response, status: :ok         
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    private
    def user_nick
        params.require(:nick).permit(:name)
    end

    def set_project
        @project = Project.find(params[:project_id])
    end
end

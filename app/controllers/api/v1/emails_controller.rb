require 'csv'

class Api::V1::EmailsController < ApplicationController
    before_action :set_project, only: [:emails, :mycommits]
    before_action :user_email, only: [:my_emails]

    # GET /projects/1/emails
    api :GET, '/projects/:project_id/emails', 'Get an array with all the emails of the uploaded emailList'
    returns :code => 200 do 
        property :projectId, :number, desc: 'Id of the related Project'
        property :emailId, String, desc: 'Id of the email in the list'
        property :senderEmail, String, desc: 'Email of the author'
        property :senderName, String, desc: 'Name of the sender'
        property :timestampReceived, String, desc: "Date of the email delivery"
        property :subject, String, desc: 'Subject of the email message'
        property :url, String, desc: 'URL to the raw message'
        property :replyto, String, desc: 'URL to the raw message that this email was a reply'

    end
    def emails
        emails = Email.where projectId: @project.id
        render json: emails, status: :ok
    end


    # GET /projects/1/commits/statistics
    api :GET, '/projects/:project_id/emails/statistics',  'Get statistics of the email list'
    returns :code => 200 do 
        property :total_emails, :number, desc: 'Total number of emails from the list'
        property :distinct_senders, :number, desc: 'Number of distinct senders'
        property :total_by_senders, Hash, desc: 'List of every sender and its number of emails'
    end
    def statistics
        contributors = Email.group(:senderEmail).count
        total_distinct = contributors.length
        response = {
            :total_emails => Email.count,
            :distinct_senders => total_distinct,
            :total_by_sender => contributors,
        }
        render json: response, status: :ok
    end

    api :POST, '/projects/:project_id/myemails', 'Get an array with all the emails from specific sender'
    returns :code => 200 do 
        property :projectId, :number, desc: 'Id of the related Project'
        property :emailId, String, desc: 'Id of the email in the list'
        property :senderEmail, String, desc: 'Email of the author'
        property :senderName, String, desc: 'Name of the sender'
        property :timestampReceived, String, desc: "Date of the email delivery"
        property :subject, String, desc: 'Subject of the email message'
        property :url, String, desc: 'URL to the raw message'
        property :replyto, String, desc: 'URL to the raw message that this email was a reply'
    end
    def myEmails
        emails = Email.where senderEmail: user_email["author_email"]
        render json: emails, status: :ok
    end

    private
    def user_email
        params.require(:email).permit(:author_email)
    end

    def set_project
        @project = Project.find(params[:project_id])
    end
end

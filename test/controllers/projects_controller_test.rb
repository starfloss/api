require 'test_helper'
require 'webmock/test_unit'

class ProjectsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @proj = projects(:one)
  end

  test "should get index" do 
    get api_v1_projects_url
    assert_response :success
  end

  test "should show Project" do 
    get api_v1_project_url @proj.id
    assert_response :success
  end

=begin
  test "should create a project" do
    WebMock.stub_request(:any, ENV['BASE_URL'] + "repositories")
    .to_return(body: {
      "name": "name",
      "rid": 1,
      "url": "http://git.example.com/foobar",
      "status": "clonning",
      "last_update": "2000-01-23 04:56:07 -0200"
    }.to_json, status: 200)

    assert_difference('Project.count') do 
      post api_v1_projects_url, params: 
      { project: { 
          project_name: 'Test',
          git_url: 'url.com.br', 
        }
      }
    end
  end
=end

  test "should destroy project" do
    WebMock.stub_request(:any, ENV['BASE_URL'] + "commits/api/v1/repositories/#{@proj.git_id}")
    .to_return(status: 200)
    delete api_v1_project_url(@proj)
    assert_response 204
  end

  test "should update article" do
    patch api_v1_project_url(@proj), params: { project: { project_name: "Testing", git_url: "url.test", git_id: 3 } }  
    @proj.reload
    assert_equal "Testing", @proj.project_name
  end

end




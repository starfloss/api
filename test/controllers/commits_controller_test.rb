require 'test_helper'
require 'webmock/test_unit'

class CommitsControllerTest < ActionDispatch::IntegrationTest

    setup do
        @proj = projects(:one)
    end
    
    test "should get commits index" do 
        WebMock.stub_request(:get, ENV['BASE_URL'] + "commits/api/v1/commits?rid=#{@proj.git_id}")
        .to_return(status: 200)
        get api_v1_project_commits_url @proj.id
        assert_response :success
    end

    test "should get commits statistics" do 
        WebMock.stub_request(:get, ENV['BASE_URL'] + "commits/api/v1/commits?rid=#{@proj.git_id}")
        .to_return(body: {
        }.to_json, status: 200)
        get api_v1_project_commits_statistics_url @proj.id
        assert_response :success
    end
    
    test "should get commits diffs" do 
        WebMock.stub_request(:get, ENV['BASE_URL'] + "commits/api/v1/commits/diffs?rid=#{@proj.git_id}")
        .to_return(body: {
        }.to_json, status: 200)
        get api_v1_project_commits_diffs_url @proj.id
        assert_response :success
    end
=begin
    test "should get commits from a user_email" do 
        WebMock.stub_request(:get, ENV['BASE_URL'] + "mycommits?rid=#{@proj.git_id}&uemail=felipe.caetano@gmail.com")
        .to_return(body: [
            {
                "id": "cf1ea74605fc4faecfca4f850d368d45f94dcae6",
                "date": "2019-07-01T20:48:10+00:00",
                "author_email": "felipe.caetano@gmail.com",
                "commiter_name": "Felipe Caetano",
                "subject": "Fix message saving function call"
            }, 
            {
                "id": "cf1ea74605fc4faecfca4f850d368d45f94dcae6",
                "date": "2019-07-01T20:48:10+00:00",
                "author_email": "felipe.caetano@gmail.com",
                "commiter_name": "Felipe Caetano",
                "subject": "Return new response for requests"
            }
        ].to_json, status: 200)
        params = 
        { 
            "email": {
                "email": {
                    "author_email": "felipe.caetano@gmail.com"
                }
            }
        }.to_json
        post api_v1_project_mycommits_url @proj.id, params
        assert_response :success
    end
=end
end



